package com.app.naapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.naapplication.R;
import com.app.naapplication.model.meetings_model;
import com.app.naapplication.utility.AppPreferences;
import com.app.naapplication.utility.CalculateDistanceTime;
import com.app.naapplication.utility.Constants;
import com.app.naapplication.utility.DataContext;
import com.google.android.gms.maps.model.LatLng;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MeetingAdapter extends  RecyclerView.Adapter<MeetingAdapter.ViewHolder> {


    public List<meetings_model> list;
    Context context;
    public String name;
    AppPreferences appPreferences;
    DataContext dataContext;





    public MeetingAdapter(Context context, List<meetings_model> list) {
        this.context = context;
        this.list = list;
        appPreferences=new AppPreferences(context);
        dataContext=new DataContext(context);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        public TextView name, location_text, location_street,location_info,tags,tvkm;
        public Button showMap;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            location_text = view.findViewById(R.id.location_text);
            location_street = view.findViewById(R.id.location_street);
            location_info=view.findViewById(R.id.location_info);
            tags=view.findViewById(R.id.tags);
            tvkm=view.findViewById(R.id.tvkm);
            showMap=view.findViewById(R.id.showMap);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.meetings_row, parent, false);

        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // setFadeAnimation(holder.itemView);

        final meetings_model task = list.get(position);
        try {
            holder.name.setText(task.MeetingName +" ("+task.Meeting_StartTime.substring(0,task.Meeting_StartTime.length()-3)+" - "+Constants.TimeDifference(task.Meeting_StartTime.substring(0,task.Meeting_StartTime.length()-3),task.MeetingDurationTime.substring(0,task.MeetingDurationTime.length()-3))+")");
            if(!task.MeetingLocationText.isEmpty()) {
                holder.location_text.setVisibility(View.VISIBLE);
                holder.location_text.setText(task.MeetingLocationText);
            }else{
                holder.location_text.setVisibility(View.GONE);
            }
            String data=task.MeetingLocationStreet;
            if(!task.MeetingNeighbourHood.isEmpty()){
                data=data+" , "+task.MeetingNeighbourHood;
            }if(!task.MeetingCity.isEmpty()){
                data=data+" , "+task.MeetingCity;
            }
            holder.location_street.setText(data);
            holder.location_info.setText(task.LocationInfo);
            holder.tags.setText(Constants.getFormatName(task.MeetingsFormat,dataContext));
            if(!task.MeetingsLatitude.isEmpty() && !task.MeetingLongitude.isEmpty() && !appPreferences.getString("latitude").isEmpty() && !appPreferences.getString("longitude").isEmpty()){
                CalculateDistanceTime distance_task = new CalculateDistanceTime(context);

                LatLng l1=new LatLng(Double.parseDouble(appPreferences.getString("latitude")),Double.parseDouble(appPreferences.getString("longitude")));
                LatLng l2=new LatLng(Double.parseDouble(task.MeetingsLatitude),Double.parseDouble(task.MeetingLongitude));

                distance_task.getDirectionsUrl(l1, l2);

                distance_task.setLoadListener(new CalculateDistanceTime.taskCompleteListener() {
                    @Override
                    public void taskCompleted(String[] time_distance) {
                        //approximate_time.setText("" + time_distance[1]);
                        holder.tvkm.setText("" + time_distance[0]);
                    }

                });
            }
                    //holder.tvkm.setText(Constants.distance(Double.parseDouble("-36.885743"),Double.parseDouble("174.739201"),Double.parseDouble(task.MeetingsLatitude),Double.parseDouble(task.MeetingLongitude)));
//            holder.tvkm.setText(Constants.distance(Double.parseDouble(appPreferences.getString("latitude")),Double.parseDouble(appPreferences.getString("longitude")),Double.parseDouble(task.MeetingsLatitude),Double.parseDouble(task.MeetingLongitude)));
//                holder.tvkm.setText(Constants.getKmFromLatLong(Double.parseDouble(task.MeetingsLatitude),Double.parseDouble(task.MeetingLongitude),Double.parseDouble(appPreferences.getString("latitude")),Double.parseDouble(appPreferences.getString("longitude"))));



            holder.showMap.setOnClickListener(v -> {
                try{

                    String strUri = "http://maps.google.com/maps?q=loc:" + task.MeetingsLatitude + "," + task.MeetingLongitude + " (" + task.MeetingLocationText + ")";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");

                    context.startActivity(intent);
                }catch (Exception ex){

                }
            });
        }catch (Exception ex){

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }
}
