package com.app.naapplication.ui;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.app.naapplication.R;
import com.app.naapplication.utility.AppPreferences;
import com.app.naapplication.utility.DataContext;
import com.app.naapplication.utility.LocationProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BaseActivity extends AppCompatActivity implements LocationProvider.LocationCallback {

    DataContext dataContext;
    AppPreferences appPreferences;
    LocationProvider locationProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataContext=new DataContext(this);
        appPreferences=new AppPreferences(this);
        locationProvider=new LocationProvider(this,this);
        locationProvider.connect();
    }

    public void StatusBar() {
        Window window = getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }


    @Override
    public void handleNewLocation(Location location) {
        appPreferences.set("latitude",location.getLatitude()+"");
        appPreferences.set("longitude",location.getLongitude()+"");

        appPreferences.set("city",getCityName(location.getLatitude(),location.getLongitude())+"");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(locationProvider!=null)locationProvider.disconnect();
    }

    public String getCityName(double lat,double lng){
        String cityName ="";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            if(addresses.size()>0) {
                cityName = addresses.get(0).getLocality();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("CITY NAME===>"+cityName);
        return cityName;
    }
}
