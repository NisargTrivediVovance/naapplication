package com.app.naapplication.utility;



import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.android.volley.toolbox.HttpResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Constants {

    public static String MEETING_API_URL="https://www.eops.in/nzna/api/meetings/";
    public static String AREA_API_URL="https://www.eops.in/nzna/api/areas/";
    public static String FORMAT_API_URL="https://www.eops.in/nzna/api/formats/";

    private static DateFormat dateFormatFirst = new SimpleDateFormat("yyyy-MM-dd");
    private static DateFormat dateFormatPDF = new SimpleDateFormat("dd/MM/yyyy");
    private static DateFormat dateFormatSecond = new SimpleDateFormat("dd MMM yyyy");
    private static DateFormat dateFormatThird = new SimpleDateFormat("MM/dd/yyyy");

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

    private static DateFormat dateFormat_notification = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private static DateFormat dateFormatTwo = new SimpleDateFormat("hh:mm a");
    private static DateFormat dateFormatThree = new SimpleDateFormat("dd MMM yyyy");


    private static DateFormat dd_mm_yyyy = new SimpleDateFormat("dd/MM/yyyy");



    public static String converDate(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormatFirst.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatSecond.format(myDate);
    }
    public static String converDatePDF(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormatPDF.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatSecond.format(myDate);
    }

    public static String converDate_YYYYMMDD(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormatSecond.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatFirst.format(myDate);
    }

    public static String converDateProfile(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormatThird.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatSecond.format(myDate);
    }

    public static String converDateProfileTwo(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormatSecond.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatThird.format(myDate);
    }


    public static String convert(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormat.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatTwo.format(myDate);
    }
    public static String convertNotification(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormat.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatTwo.format(myDate);
    }

    public static String convertNotificationTwo(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormat.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatFirst.format(myDate);
    }

    public static String convertTwo(String currentDate){
        Date myDate = null;
        try {
            myDate = dateFormat.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatThree.format(myDate);
    }


    public static String addMinTime(String time, int amount){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        Date d = null;
        try {
            d = df.parse(time);
            cal.setTime(d);
            cal.add(Calendar.MINUTE, amount);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String newTime = df.format(cal.getTime());
        return newTime;
    }

    public static String addDays(String time, int amount){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = df.parse(time);
            cal.setTime(d);
            cal.add(Calendar.DAY_OF_MONTH, amount);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String newTime = df.format(cal.getTime());
        return newTime;
    }

    public static String addDaysBooking(String time, int amount){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;
        try {
            d = df.parse(time);
            cal.setTime(d);
            cal.add(Calendar.DAY_OF_MONTH, amount);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String newTime = df.format(cal.getTime());
        return newTime;
    }

    public static String cuurentTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        Date d = cal.getTime();
        try {
            d = df.parse(cal.getTime().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String newTime = df.format(d);
        return newTime;
    }

    public static int getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        return age>0?age:0;
    }

    public static Date getWeekStartDate() {
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, -1);
        }
        return calendar.getTime();
    }

    public static Date getWeekEndDate() {
        Calendar calendar = Calendar.getInstance();
        if(calendar.get(Calendar.DAY_OF_WEEK)== Calendar.MONDAY){
            while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                calendar.add(Calendar.DATE, 1);
            }
            //calendar.add(Calendar.DATE, 1);
        }else if(calendar.get(Calendar.DAY_OF_WEEK)!= Calendar.MONDAY){
            while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
                calendar.add(Calendar.DATE, 1);
            }
            calendar.add(Calendar.DATE, -1);
        }

        return calendar.getTime();
    }

    public static ArrayList<String> getPreviousYears() {
        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 2000; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        return years;
    }

    public static String getCurrentMonthName() {
        String month_name="";
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
             month_name= month_date.format(cal.getTime());
        }catch (Exception ex){
            System.out.println("getCurrentMonthName()===>"+ex.toString());
        }
        return month_name;
    }

    public static int currentQuarter(){
        int quarter=0;
        try {
            Calendar cal = Calendar.getInstance(Locale.US);
            int month = cal.get(Calendar.MONTH);
             quarter= (month / 3) + 1;
            System.out.println("Quarter = " + quarter);
        }catch (Exception ex){
            System.out.println("Current Quarter Error"+ex.toString());
        }
        return quarter;
    }

    public static boolean validateJavaDate(String strDate)
    {
        /* Check if date is 'null' */
        if (strDate.trim().equals(""))
        {
            return true;
        }
        /* Date is not 'null' */
        else
        {
            /*
             * Set preferred date format,
             * For example MM-dd-yyyy, MM.dd.yyyy,dd.MM.yyyy etc.*/
            SimpleDateFormat sdfrmt = new SimpleDateFormat("MM/dd/yyyy");
            sdfrmt.setLenient(false);
            /* Create Date object
             * parse the string into date
             */
            try
            {
                Date javaDate = sdfrmt.parse(strDate);
                System.out.println(strDate+" is valid date format");
            }
            /* Date format is invalid */
            catch (ParseException e)
            {
                System.out.println(strDate+" is Invalid Date format");
                return false;
            }
            /* Return true if date format is valid */
            return true;
        }
    }

    public static String TimeDifference(String sttime,String endtime){
        String diff="";
        try
        {

            String myTime = sttime;
            String[] etime=endtime.split(":");
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            Date d = df.parse(myTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            if(endtime.length()>0){
                cal.add(Calendar.HOUR, Integer.parseInt(etime[0]));
                cal.add(Calendar.MINUTE, Integer.parseInt(etime[1]));
            }

            diff = df.format(cal.getTime());

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return diff;
    }

    public static String getFormatName(String formats,DataContext dataContext){

        String name="";
        try {
            if(!formats.isEmpty()) {
                String[] data = formats.split(",");
                for (int i = 0; i < data.length; i++) {
                        dataContext.formats_models.fill("key_string = ?",new String[]{data[i]},null);
                        if(dataContext.formats_models.size()>0) {
                            if (i > 0)
                                name = name + "," + dataContext.formats_models.get(0).KeyName;
                            else
                                name = dataContext.formats_models.get(0).KeyName;
                        }

                }
            }

        }catch (Exception ex){

        }
        return name;
    }


    public static String distance(double lat1, double lon1, double lat2, double lon2) {
        double dist=0;
        try {
            double theta = lon1 - lon2;
            dist = Math.sin(deg2rad(lat1))
                    * Math.sin(deg2rad(lat2))
                    + Math.cos(deg2rad(lat1))
                    * Math.cos(deg2rad(lat2))
                    * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            System.out.println(dist + "DISTANCE===============>");

        } catch (Exception ex) {

        }
        return (String.format("%.2f", dist) + " KM");
    }

        public static double deg2rad ( double deg){
            return (deg * Math.PI / 180.0);
        }

        public static double rad2deg ( double rad){
            return (rad * 180.0 / Math.PI);
        }


    public static String getKmFromLatLong(double lat1, double lng1, double lat2, double lng2){

        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat2-lat1);
        double lngDiff = Math.toRadians(lng2-lng1);
        double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(lngDiff /2) * Math.sin(lngDiff /2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return (new Float(distance).floatValue())+"";

    }

    public static String distance(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist+"";
    }
}
