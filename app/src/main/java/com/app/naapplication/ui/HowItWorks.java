package com.app.naapplication.ui;

import android.widget.TextView;

import com.app.naapplication.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.how_it_works)
public class HowItWorks extends BaseActivity {

    @ViewById
    TextView tvWhatisNA;
    @AfterViews
    public void init(){
        getSupportActionBar().show();
        // String text=getResources().getString(R.string.what_is_na);
         //   tvWhatisNA.setText(Html.fromHtml(text));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
