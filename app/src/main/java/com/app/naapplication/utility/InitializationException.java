package com.app.naapplication.utility;

public class InitializationException extends Throwable {

    public String message;

    public InitializationException(String message) {
        this.message = message;
    }
}
