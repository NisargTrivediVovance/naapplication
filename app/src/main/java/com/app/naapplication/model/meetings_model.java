package com.app.naapplication.model;

import com.google.gson.annotations.SerializedName;
import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;
import com.mobandme.ada.annotations.TableField;

import java.io.Serializable;

import static com.mobandme.ada.Entity.DATATYPE_STRING;

@Table(name = "tbl_meeting")
public class meetings_model extends Entity implements Serializable {

    @SerializedName("start_time")
    @TableField(name = "start_time",datatype = DATATYPE_STRING)
    public String Meeting_StartTime="";

    @SerializedName("duration_time")
    @TableField(name = "duration_time",datatype = DATATYPE_STRING)
    public String MeetingDurationTime="";

    @SerializedName("meeting_name")
    @TableField(name = "meeting_name",datatype = DATATYPE_STRING)
    public String MeetingName="";


    @SerializedName("location_text")
    @TableField(name = "location_text",datatype = DATATYPE_STRING)
    public String MeetingLocationText="";

    @SerializedName("location_neighborhood")
    @TableField(name = "location_neighborhood",datatype = DATATYPE_STRING)
    public String MeetingNeighbourHood="";


    @SerializedName("location_street")
    @TableField(name = "location_street",datatype = DATATYPE_STRING)
    public String MeetingLocationStreet="";

    @SerializedName("formats")
    @TableField(name = "formats",datatype = DATATYPE_STRING)
    public String MeetingsFormat="";


    @SerializedName("longitude")
    @TableField(name = "longitude",datatype = DATATYPE_STRING)
    public String MeetingLongitude="";


    @SerializedName("latitude")
    @TableField(name = "latitude",datatype = DATATYPE_STRING)
    public String MeetingsLatitude="";


    @SerializedName("weekday_tinyint")
    @TableField(name = "weekday_tinyint",datatype = DATATYPE_STRING)
    public String MeetingDay="";


    @SerializedName("location_municipality")
    @TableField(name = "location_municipality",datatype = DATATYPE_STRING)
    public String MeetingCity="";

    @SerializedName("location_info")
    @TableField(name = "location_info",datatype = DATATYPE_STRING)
    public String LocationInfo="";





}

//
//"id_bigint": "176",
//        "worldid_mixed": "G00377820",
//        "service_body_bigint": "2",
//        "weekday_tinyint": "6",
//        "start_time": "00:30:00",
//        "duration_time": "01:00:00",
//        "formats": "O,WC",
//        "longitude": "178.0238074",
//        "latitude": "-38.6634468",
//        "meeting_name": "Gisborne Friday Meeting",
//        "location_text": "Holy Trinity Church",
//        "location_info": "The meeting is not in the church but in other  building on site.  Look for sign",
//        "location_street": "70 Derby Street",
//        "location_neighborhood": "",
//        "location_municipality": "Gisborne",
//        "location_sub_province": "",
//        "location_province": "",
//        "location_postal_code_1": "4010",
//        "comments": "",
//        "root_server_uri": "https://nzna.org/bmlt/main_server",
//        "format_shared_id_list ": null