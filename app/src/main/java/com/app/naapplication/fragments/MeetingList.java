package com.app.naapplication.fragments;

import android.app.ProgressDialog;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.naapplication.R;
import com.app.naapplication.adapter.MeetingAdapter;
import com.app.naapplication.model.formats_model;
import com.app.naapplication.model.meetings_model;
import com.app.naapplication.parser.FormatParser;
import com.app.naapplication.parser.MeetingsParser;
import com.app.naapplication.utility.AppPreferences;
import com.app.naapplication.utility.Constants;
import com.app.naapplication.utility.DataContext;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobandme.ada.Entity;
import com.mobandme.ada.exceptions.AdaFrameworkException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class MeetingList extends Fragment  {

    Spinner spCity,spDays;
    RecyclerView rcMeetings;
    ArrayList<meetings_model> meetings_models=new ArrayList<>();
    SwipeRefreshLayout swipe;
    DataContext dataContext;
    MeetingAdapter adapter;
    ArrayList<String> cityList=new ArrayList<>();
    TextView tvmsg;
    AppPreferences appPreferences;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dataContext=new DataContext(getActivity());
        rcMeetings=view.findViewById(R.id.rcMeetings);
        spCity=view.findViewById(R.id.spCity);
        spDays=view.findViewById(R.id.spDays);
        tvmsg=view.findViewById(R.id.tvmsg);
        swipe=view.findViewById(R.id.swipe);
        appPreferences=new AppPreferences(getActivity());
        try {
            dataContext.formats_models.fill();
            if(dataContext.formats_models.size()>0)
                bindMeetings();
            else
                bindFormats();
        } catch (AdaFrameworkException e) {
            e.printStackTrace();
        }

        adapter=new MeetingAdapter(getActivity(),dataContext.meetingsModels);
        rcMeetings.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcMeetings.setAdapter(adapter);
        setDayFilter();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                bindMeetings();
                swipe.setRefreshing(false);
            }
        });
    }
    private void setMeetingTotal(int i){
        tvmsg.setText("We have a total of "+i+" meetings listed.");
    }
    private void setDayFilter(){


        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        spDays.setSelection(getDayIndex(dayOfTheWeek));

        spDays.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String data= (String) adapterView.getItemAtPosition(i);
                System.out.println("DATA---->"+data);
                try {
                    dataContext.meetingsModels.fill("weekday_tinyint = ? and location_municipality = ?",new String[]{daysFilter(i)+"",spCity.getSelectedItem().toString()},"start_time");
                    adapter.notifyDataSetChanged();
                    setMeetingTotal(adapter.getItemCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private int getDayIndex(String name){
        int i=0;
        switch (name){
            case "Sunday":i=0;break;
            case "Monday":i=1;break;
            case "Tuesday":i=2;break;
            case "Wednesday":i=3;break;
            case "Thursday":i=4;break;
            case "Friday":i=5;break;
            case "Saturday":i=6;break;
        }
        return i;
    }
    private int daysFilter(int i){
        int data=0;
        switch (i){
            case 0:data=1;break;
            case 1:data=2;break;
            case 2:data=3;break;
            case 3:data=4;break;
            case 4:data=5;break;
            case 5:data=6;break;
            case 6:data=7;break;
        }
        return data;
    }

    private void bindCity(){
        try {
            dataContext.meetingsModels.fill();
            if(dataContext.meetingsModels.size()>0) {

                for (meetings_model model : dataContext.meetingsModels) {
                    if (!cityList.contains(model.MeetingCity))
                        cityList.add(model.MeetingCity);
                }

                final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                        (getActivity(), android.R.layout.simple_spinner_item,
                                cityList); //selected item will look like a spinner set from XML
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                        .simple_spinner_dropdown_item);
                spCity.setAdapter(spinnerArrayAdapter);


                for (int i=0;i<dataContext.meetingsModels.size();i++) {
                   if(appPreferences.getString("city").equalsIgnoreCase(dataContext.meetingsModels.get(i).MeetingCity)){
                        spCity.setSelection(i);
                        break;
                   }
                }
                spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String data = (String) spinnerArrayAdapter.getItem(i);
                        System.out.println("DATA---->" + data);
                        filterData(data);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

        } catch (AdaFrameworkException e) {
            e.printStackTrace();
        }
    }

    private void filterData(String filter){
        try {
            System.out.println("FILTER DATA---->"+filter);
            dataContext.meetingsModels.fill("location_municipality = ? and weekday_tinyint = ?",new String[]{filter,daysFilter(spDays.getSelectedItemPosition())+""},"start_time");
            adapter.notifyDataSetChanged();
            setMeetingTotal(adapter.getItemCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.meetings_list, null);
        return v;
    }

    private void bindMeetings(){
        final ProgressDialog dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Load meetings...");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest request=new StringRequest(
                Request.Method.GET,
                Constants.MEETING_API_URL,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(dialog.isShowing())
                    dialog.dismiss();
                VolleyLog.wtf(response, "utf-8");
                GsonBuilder builder = new GsonBuilder();
                Gson mGson = builder.create();
                MeetingsParser meetings = mGson.fromJson(response, MeetingsParser.class);
                meetings_models.addAll(meetings.list);
                addData(meetings_models);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(dialog.isShowing())
                    dialog.dismiss();
            }
        }){

        };
        RequestQueue queue= Volley.newRequestQueue(getActivity());
        queue.add(request);
    }
    private void addData(ArrayList<meetings_model> list){
        try {
            dataContext.meetingsModels.fill();
            for(int i=0;i<dataContext.meetingsModels.size();i++)
                dataContext.meetingsModels.remove(i).setStatus(Entity.STATUS_DELETED);

            dataContext.meetingsModels.save();
            dataContext.meetingsModels.addAll(list);
            dataContext.meetingsModels.save();
            adapter.notifyDataSetChanged();
            setMeetingTotal(adapter.getItemCount());
            bindCity();
        } catch (AdaFrameworkException e) {
            e.printStackTrace();
        }
    }


    private void bindFormats(){
        final ProgressDialog dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Load meetings...");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest request=new StringRequest(
                Request.Method.GET,
                Constants.FORMAT_API_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(dialog.isShowing())
                            dialog.dismiss();
                        VolleyLog.wtf(response, "utf-8");
                        GsonBuilder builder = new GsonBuilder();
                        Gson mGson = builder.create();
                        FormatParser meetings = mGson.fromJson(response, FormatParser.class);
                        addFormats(meetings.list);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(dialog.isShowing())
                    dialog.dismiss();
            }
        }){

        };
        RequestQueue queue= Volley.newRequestQueue(getActivity());
        queue.add(request);
    }
    private void addFormats(ArrayList<formats_model> formats_models){
        try {
            dataContext.formats_models.fill();
            for(int i=0;i<dataContext.formats_models.size();i++)
                dataContext.formats_models.remove(i).setStatus(Entity.STATUS_DELETED);

            dataContext.formats_models.save();
            dataContext.formats_models.addAll(formats_models);
            dataContext.formats_models.save();

            bindMeetings();
        } catch (AdaFrameworkException e) {
            e.printStackTrace();
        }
    }


}
