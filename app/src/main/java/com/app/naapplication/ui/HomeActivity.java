package com.app.naapplication.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.app.naapplication.R;
import com.app.naapplication.adapter.DrawerItemCustomAdapter;
import com.app.naapplication.fragments.MeetingList;

import com.app.naapplication.model.DataModel;
import com.app.naapplication.utility.DrawerMenu;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.nav_drawer)
public class HomeActivity extends BaseActivity {

    @ViewById(R.id.drawerlayout)
    DrawerLayout drawerlayout;

    @ViewById(R.id.Menu)
    ImageView Menu;

    @ViewById(R.id.left_drawer)
    ListView left_drawer;

    @ViewById(R.id.content_frame)
    FrameLayout content_frame;

    @ViewById
    TextView tvTitle;


    private String[] mNavigationDrawerItemTitles;
    ActionBarDrawerToggle actionBarDrawerToggle;

    @AfterViews
    public void init(){

        StatusBar();
        bindMenu();
        loadiniitFragement();
    }
    private void loadiniitFragement(){

        Fragment fragment = new MeetingList();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            left_drawer.setItemChecked(0, true);
            left_drawer.setSelection(0);
            //setTitle(mNavigationDrawerItemTitles[position]);
            drawerlayout.closeDrawer(GravityCompat.START);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }
    private void bindMenu(){
        DataModel[] drawerItem = new DataModel[3];
        drawerItem[0] = new DataModel(R.drawable.ic_view_list_black_24dp, DrawerMenu.MENU_ONE);
        drawerItem[1] = new DataModel(R.drawable.ic_view_list_black_24dp, DrawerMenu.MENU_TWO);
        drawerItem[2] = new DataModel(R.drawable.ic_view_list_black_24dp, DrawerMenu.MENU_THREE);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //getSupportActionBar().setHomeButtonEnabled(true);

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.listview_row, drawerItem);
        left_drawer.setAdapter(adapter);
        left_drawer.setOnItemClickListener(new DrawerItemClickListener());
        drawerlayout.setDrawerListener(actionBarDrawerToggle);
        setupDrawerToggle();

    }
    @Click
    public void Menu(){
        drawerlayout.openDrawer(Gravity.LEFT);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }

    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new MeetingList();
                break;
            case 1:
                //fragment = new FixturesFragment();
                drawerlayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,WhatisNA_.class));
                break;
            case 2:
                drawerlayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,HowItWorks_.class));
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            left_drawer.setItemChecked(position, true);
            left_drawer.setSelection(position);
            //setTitle(mNavigationDrawerItemTitles[position]);
            drawerlayout.closeDrawer(GravityCompat.START);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    void setupToolbar(){
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    void setupDrawerToggle(){
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerlayout,null,R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        actionBarDrawerToggle.syncState();
    }



}
