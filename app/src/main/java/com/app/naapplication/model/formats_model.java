package com.app.naapplication.model;

import com.google.gson.annotations.SerializedName;
import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;
import com.mobandme.ada.annotations.TableField;

@Table(name = "formats")
public class formats_model extends Entity {

    @SerializedName("key_string")
    @TableField(name = "key_string",datatype = DATATYPE_STRING)
    public String KeyString="";

    @SerializedName("name_string")
    @TableField(name = "name_string",datatype = DATATYPE_STRING)
    public String KeyName="";
}
