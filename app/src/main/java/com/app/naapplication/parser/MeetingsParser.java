package com.app.naapplication.parser;

import com.app.naapplication.model.meetings_model;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MeetingsParser implements Serializable {

    @SerializedName("code")
    public int Code=0;

    @SerializedName("message")
    public String Message="";

    @SerializedName("data")
    public ArrayList<meetings_model> list=new ArrayList<>();

}
